# Choice_of_bayesian_model


In the R file provided there are some codes about :


- Choice of a bayesian model using H0 and H1 hypothesis (mean, sd, sqrt, pt, t.test)



- Test to compare two means with chickwts data (plot, t.test, var.test)



- Equality test of a proportion according to a certain value (prop.test)



- Test with bayesian hypothesis. Instead of using H0 and H1 we formulate those hypothesis in a different way in order to have 2 alternatives and chose between 2 models (log, dbeta, curve, dlogis, proportionBF)



- Example for a mean test according to a standard (ttestBF, hcubature)



- Equality bayesian test of 2 mean (ttestBF, log10)



- A few explanation about bayesian regression 


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

October 2021

